<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Lösenordet måste minst vara sex tecken och matcha bekräftelsefältet.',
    'reset' => 'Ditt lösenord har återställts!',
    'sent' => 'Ett mail har skickats till din e-postadress så att du kan återställa lösenordet.',
    'token' => 'Återställningskoden är felaktig.',
    'user' => "Vi kan inte hitta en användare med den e-postadressen.",

];
