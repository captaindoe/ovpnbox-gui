@extends('master')

@section('content')

    <div class="col-sm-6">
        <p class="text">
            {{ trans('auth.login_to_access') }}
        </p>
        <p class="text">
            {{ trans('auth.supply_credentials') }}
        </p>
    </div>
    <div class="col-sm-6">
        <form role="form" method="post" id="login">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="username">{{ trans('auth.username') }}</label>
                <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" />
            </div>
            <div class="form-group">
                <label for="password">{{ trans('auth.password') }}</label>
                <input type="password" class="form-control" id="password" name="password" />
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" id="remember"> {{ trans('auth.remember_me') }}
                    </label>
                </div>
            </div>
            <div class="form-group text-center">
                <br />
                <button type="submit" name="submit" class="btn btn-default">{{ trans('auth.sign_in') }}</button>
            </div>
        </form>
    </div>
@endsection