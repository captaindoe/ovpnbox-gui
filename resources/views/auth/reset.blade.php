@extends('master')

@section('content')

    <div class="col-sm-6">
        <p class="text">
            Reset the password for the administrator account on the OVPNbox.
        </p>
    </div>
    <div class="col-sm-6">
        <form role="form"  method="POST" action="/password/reset">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">

            @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <div class="form-group">
                <label for="email">E-mail address</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" />
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">Confirm Password</label>
                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-default">
                    Reset Password
                </button>
            </div>
        </form>
    </div>
@endsection