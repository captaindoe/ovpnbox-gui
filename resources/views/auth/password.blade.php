@extends('master')

@section('content')

    <div class="col-sm-6">
        <p class="text">
            Reset the password for the administrator account on the OVPNbox.
        </p>
    </div>
    <div class="col-sm-6">
        <form role="form" id="reset_form">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="email">E-mail address</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" />
            </div>
            <div class="form-group text-center">
                <br />
                <button type="submit" name="submit" class="btn btn-default">Send Password Reset Link</button>
            </div>
        </form>
    </div>
@endsection