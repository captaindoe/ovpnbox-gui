@extends('master')

@section('content')

    <div id="token" data-token="{{ csrf_token() }}"></div>
    <h4>{{ trans('stats.statistics') }}</h4>
    @if($connected->isEmpty())
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-info" role="alert">{{ trans('stats.no_connected_groups') }}</div>
            </div>
        </div>
    @endif

    @if(!$connected->isEmpty())
        @foreach($connected as $group)
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="min-height: 60px;">
                            <div class="row">
                                <div class="col-sm-7">
                                    <span class="group-title">{{ $group->name }}</span><br />
                                    @if($group->adapter->connected)
                                        <span id="{{ $group->adapter->name }}-hours">hh</span>:<span id="{{ $group->adapter->name }}-minutes">mm</span>:<span id="{{ $group->adapter->name }}-seconds">ss</span>
                                    @else
                                        <span class="label label-danger">{{ trans('stats.disconnected') }}</span>
                                    @endif
                                </div>
                                <div class="col-sm-5 text-right" style="margin-top: 6px;">
                                    <button type="button" class="btn btn-default btn-sm graph" data-group="{{ $group->id }}" data-interval="day">
                                        {{ trans('stats.one_day') }}</button>
                                    &nbsp;
                                    <button type="button" class="btn btn-default btn-sm graph" data-group="{{ $group->id }}" data-interval="week">
                                        {{ trans('stats.one_week') }}
                                    </button>
                                    &nbsp;
                                    <button type="button" class="btn btn-default btn-sm graph" data-group="{{ $group->id }}" data-interval="month">
                                        {{ trans('stats.one_month') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <h4 class="text-center" style="margin-bottom: 10px;" id="title_{{ $group->id }}">{{ trans('stats.one_day') }}</h4>
                            @if(is_null($group->traffic()->first()))
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-info text-center" role="alert">{{ trans('stats.no_statistics_yet') }}</div>
                                    </div>
                                </div>
                            @else
                                <div class="text-center" style="margin-bottom: 20px;">
                                    <span style="color:#1EB300;margin-left: 10px;"><i class="fa fa-arrow-down"></i>&nbsp;<span id="traffic_input_{{ $group->id }}">{{ number_format($group->traffic()->orderBy('id', 'desc')->value('total_in_megabytes'), 0, ',', ' ') }}</span> MB</span>
                                    <span style="color:#6D88AD"><i class="fa fa-arrow-up"></i>&nbsp;<span id="traffic_output_{{ $group->id }}">{{ number_format($group->traffic()->orderBy('id', 'desc')->value('total_out_megabytes'), 0, ',', ' ') }}</span> MB</span>
                                </div>

                                <div id="graph_{{ $group->id }}" class="hidden-xs" style="min-width: 400px; height: 300px; margin: 0 auto"></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function() {
                   loadGraph({{ $group->id }}, 'day');
                    var {{ $group->adapter->name }}_uptime = 0;
                });
            </script>
        @endforeach
    @endif
@endsection