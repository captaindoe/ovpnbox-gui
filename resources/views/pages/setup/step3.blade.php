@extends('master')

@section('content')
    <h4 class="text-center">Create an administrator account for <strong>OVPNbox</strong></h4><br />
    <div class="row">
        <div class="col-sm-6">
            <p>
               You are required to create an administrator account for your OVPNbox in order to protect it from unauthorized access from devices on your network.
            </p>
        </div>
        <div class="col-sm-6">
            <form method="post" id="create">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="name">Username</label>
                    <input type="text" class="form-control" id="name" name="name" value="admin" />
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" value="" />
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" />
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Repeat password</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" />
                </div>
                <div class="form-group">
                    <label for="language">Language</label>
                    <select class="form-control" id="language">
                        <option value="en" selected>English</option>
                        <option value="sv">Swedish</option>
                    </select>
                </div>
                <div class="form-group text-center">
                    <button type="submit" name="submit" class="btn btn-primary">Create account</button>
                </div>
            </form>
        </div>
    </div>
@endsection