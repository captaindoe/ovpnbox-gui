@extends('master')

@section('content')

    <h4 class="text-center">Welcome to OVPNbox!<br /><small>Seamless, simple and secure</small></h4><br />
    <div class="row">
        <div class="col-sm-12 text-center">
            <p>
                An introductory, but short configuration is required before you can start using your OVPNbox.
            </p>
            <form method="post" id="setup">
                {{ csrf_field() }}
                <div class="form-group text-center">
                    <button type="submit" name="submit" class="btn btn-primary" id="start">Begin configuration</button>
                </div>
            </form>

        </div>
    </div>
@endsection