@extends('master')

@section('content')

    <h4 class="text-center">Account credentials for <strong>OVPN</strong></h4><br />
    <div class="row">
        <div class="col-sm-6">
            <p>
                Enter the credentials for your <strong>OVPN account</strong>. Enter your username, not the email address.
            </p>
        </div>
        <div class="col-sm-6">
            <form method="post" id="login">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" />
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" />
                </div>
                <div class="form-group text-center">
                    <button type="submit" name="submit" class="btn btn-primary">Sign in</button>
                </div>
            </form>
        </div>
    </div>
@endsection