@extends('master')

@section('content')

<h4>
    <span class="pull-left" style="margin-bottom: 30px;">{{ trans('ovpn.account') }}</span>
    <span class="pull-right">
        <a href="javascript:void(0);" class="reload_settings" title="{{ trans('ovpn.refresh_account') }}" data-token="{{ csrf_token() }}"><i class="fa fa-refresh"></i>
        </a>
    </span>
</h4>
<table class="table table-hover">
    <tbody>
    <tr>
        <td>{{ trans('ovpn.username') }}</td>
        <td>{{ $account->username }}</td>
    </tr>
    <tr>
        <td>{{ trans('ovpn.subscription') }}</td>
        <td>{{ $account->default }}</td>
    </tr>
    <tr>
        <td>{{ trans('ovpn.filtration') }}</td>
        <td>{{ $account->filtering }}</td>
    </tr>
    <tr>
        <td>{{ trans('ovpn.public_ipv4') }}</td>
        <td>{{ $account->public_ipv4 }}</td>
    </tr>
    <tr>
        <td>{{ trans('ovpn.multihop') }}</td>
        <td>{{ $account->multihop }}</td>
    </tr>
    </tbody>
</table>

<h4>{{ trans('ovpn.datacenters') }}</h4>
<table class="table table-hover">
    <tbody>
        @foreach($datacenters['datacenters'] as $datacenter)
            <tr>
                <td>{{ $datacenter->city }}, {{ $datacenter->country }}</td>
                <td>{{ $datacenter->hops }} {{ trans('ovpn.hops') }} - {{ $datacenter->ping }} {{ trans('ovpn.latency') }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<h4>{{ trans('ovpn.top_servers') }}</h4>
<table class="table table-hover">
    <tbody>
    @foreach($servers as $server)
        <tr>
            <td>{{ $server->name }}</td>
            <td>{{ $server->currentLoad->bandwidth }}% {{ trans('ovpn.load') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@endsection