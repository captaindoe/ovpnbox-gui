<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOvpnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ovpn', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->integer('default')->unsigned()->nullable();
            $table->integer('proxy')->unsigned()->nullable();
            $table->integer('filtering')->unsigned()->nullable();
            $table->integer('multihop')->unsigned()->nullable();
            $table->integer('public_ipv4')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ovpn');
    }
}
