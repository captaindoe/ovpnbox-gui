<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOvpnCredentialsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('ovpn_username')->nullable();
            $table->string('ovpn_password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return voidcd stor
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['ovpn_username', 'ovpn_password']);
        });
    }
}
