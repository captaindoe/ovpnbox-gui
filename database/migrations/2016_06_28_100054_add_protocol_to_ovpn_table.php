<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProtocolToOvpnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ovpn', function (Blueprint $table) {
            $table->string('protocol', 3)->default('udp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ovpn', function (Blueprint $table) {
            $table->dropColumn('protocol');
        });
    }
}
