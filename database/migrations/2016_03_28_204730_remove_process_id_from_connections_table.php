<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveProcessIdFromConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('connections', 'process_id')) {
            Schema::table('connections', function (Blueprint $table) {
                $table->dropColumn('process_id');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('connections', 'process_id')) {
            Schema::table('connections', function (Blueprint $table) {
                $table->integer('process_id');
            });
        }
    }
}
