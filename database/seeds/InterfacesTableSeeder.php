<?php

use Illuminate\Database\Seeder;

class InterfacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Interfaces::create([
            'type' => 'lan',
            'name' => 'bridge0'
        ]);
        \App\Interfaces::create([
            'type' => 'wan',
            'name' => 'em1'
        ]);
        \App\Interfaces::create([
            'name' => 'ovpnc1',
            'type' => 'openvpn'
        ]);
        \App\Interfaces::create([
            'name' => 'ovpnc2',
            'type' => 'openvpn'
        ]);
        \App\Interfaces::create([
            'name' => 'ovpnc3',
            'type' => 'openvpn'
        ]);
        \App\Interfaces::create([
            'name' => 'ovpnc4',
            'type' => 'openvpn'
        ]);
    }
}
