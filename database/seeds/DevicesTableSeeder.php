<?php

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $list = [
            '95:b0:e3:a5:ca:18',
            'd9:8b:c0:bc:b4:68',
            'd5:c3:b1:c4:67:54',
            'af:a6:9b:e6:46:10',
            '6c:d1:03:ae:86:13',
            '8d:8e:4d:3e:cd:46',
            '82:cd:8e:de:aa:0b',
            'da:96:0a:98:71:7b',
            'ea:9b:7d:1f:fd:c7',
            'ba:56:45:12:fa:80',
            '41:8b:3f:61:9f:c0',
            'cd:0b:bc:26:67:cb',
            'dd:dc:75:cb:65:69',
            '4c:be:b7:e7:5e:20',
            'b4:61:c3:c9:09:58',
            'e2:35:7a:eb:69:92',
            '18:96:6c:61:0c:3c',
            'ff:50:9b:87:db:c4'
        ];

        $x = 10;

        foreach($list as $mac) {

            DB::table('devices')->insert([
                'mac' => $mac,
                'ip' => sprintf("%u", ip2long('10.220.0.' . $x)),
                'group_id' => rand(1,4),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
            $x++;
        }

    }
}
