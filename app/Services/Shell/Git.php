<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-03-11
 * Time: 10:22
 */

namespace App\Services\Shell;


use Cache;

class Git
{
    public static function getBoxVersion()
    {
        return trim(shell_exec('cd /usr/local/www/ovpn-gui && git rev-parse HEAD'));
    }

    public static function getBoxBranch()
    {
        return trim(shell_exec('cd /usr/local/www/ovpn-gui && git rev-parse --abbrev-ref HEAD'));
    }

    public static function isThereNewFirmwareAvailable()
    {
        return Cache::get('firmware_available', function() {
            $retval = false;
            $available = System::getAvailableFirmwareVersion();
            $current = System::getBoxFirmwareVersion();

            if($available) {
                if($available != $current) {
                    $retval = true;
                }
            }
            Cache::put('firmware_available', $retval, 60);
            return $retval;
        });
    }

    public static function isThereNewGuiAvailable()
    {
        return Cache::get('gui_available', function() {
            $retval = false;
            $available = System::getAvailableBoxVersion();
            $current = self::getBoxVersion();

            if($available) {
                if($available != $current) {
                    $retval = true;
                }
            }
            Cache::put('gui_available', $retval, 60);
            return $retval;
        });
    }
}