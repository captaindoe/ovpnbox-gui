<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-04-03
 * Time: 13:20
 */

namespace App\Services\Helpers;

use App;
use App\Group;
use App\Interfaces;
use App\Ovpn;
use App\Connection;
use App\Services\Network\Adapter;
use App\Services\Network\Socket;
use App\Services\Ovpn\Api;
use App\Services\Ovpn\Dashboard;
use App\Services\Shell\System;
use App\Traffic;
use Cache;
use Carbon\Carbon;
use Log;

class OpenVPN
{

    /**
     * Start OpenVPN process
     *
     * @param $input
     * @return array
     */
    public static function start($input)
    {
        $adapter = new Adapter();
        $openvpn = $adapter->getAvailableOpenVPN($input['group']);
        $adapter_id = String::adapterToId($openvpn);
        $interface_id = Interfaces::where('name', $openvpn)->value('id');
        $protocol = Ovpn::first()->protocol;

        if(is_null($adapter_id)) {
            return ['error' => ['adapter_fail' => [trans('dashboard.adapters_occupied')]]];
        }

        // Fetch IP for server based on user selection
        $ip = Dashboard::getIpForConnection($input['type'], $input['ip'], $input['addon']);

        if(!$ip) {
            return ['error' => ['choose_manual' => [trans('dashboard.choose_manual_instead')]]];
        }

        // Make sure the "normal" addon property always uses UDP protocol
        if (($protocol === "tcp" && $input['addon'] !== "normal")) {
            $protocol = "udp";
        }

        // Fetch ports based on selected addon
        $ports = Dashboard::getPortsForAddon($input['addon']);

        // Connect to OVPN
        Socket::send('connect', trans('dashboard.connecting_to_server'));
        $account = Ovpn::find(1);

        $pfsense = new PfSense();
        $pfsense->setOpenVPN($adapter_id, $ip, $ports, $account, $protocol);

        Socket::send('connect', trans('dashboard.verifying_connection'));

        sleep(30);

        if(!file_exists('/tmp/' . $openvpn . 'up')) {
            $pfsense->disconnectOpenVPN(($adapter_id+1));
            Log::error('OpenVPN startades inte', ['id' => ($adapter_id+1), 'interface' => $openvpn]);

            return [
                'error' => [
                    'not_started' => [
                        self::parseError($adapter_id)
                    ]
                ]
            ];
        }

        // Loop through all devices in group and add routes
        if($input['group'] != 1) {

            // Specifik kundgrupp angiven
            Socket::send('connect', trans('dashboard.openvpn_started'));
        } else {

            // Ingen grupp angiven, osorterade enheter
            Socket::send('connect', trans('dashboard.openvpn_started_2'));
        }

        if($input['killswitch'] == 'active') {
            $killswitch = true;
        } else {
            $killswitch = false;
        }

        // Update group data
        Group::find($input['group'])->update([
            'interface_id' => $interface_id,
            'killswitch' => $killswitch
        ]);

        $data = [
            'input' => $input,
            'ids' => [
                'group_id' => $input['group'],
                'interface_id' => $interface_id,
                'adapter_id' => $adapter_id,
                'vpnclient_id' => ($adapter_id+1)
            ],
            'event' => [
                'ip' => $ip,
                'addon' =>  String::addonAbbreviationToText($input['addon']),
                'up' => '/var/etc/openvpn/client' . ($adapter_id+1) . '.up',
                'log' => '/tmp/openvpn-' . $adapter_id . '.log',
                'server' => $ip,
                'ip_info' => $adapter->getStaticIP($openvpn)
            ]
        ];

        // Generate a new config.xml file
        Config::generate();
        Config::flushFirewall();

        // Fetch all of OVPNs servers
        $api = new Api();
        $servers = $api->getServers();

        if($servers) {
            foreach($servers as $list) {
                if($list->ip == $data['event']['ip']) {
                    $data['event']['server'] = $list->name;
                    break;
                }
            }
        } else {
            $data['event']['server'] = 'Unknown';
            Log::debug('Weird server return information', ['servers' => $servers, 'data_store' => $data]);
            Cache::forget('servers');
        }

        // Store connection information
        Connection::create([
            'group_id' => $data['ids']['group_id'],
            'interface_id' => $data['ids']['interface_id'],
            'directives' => $data
        ]);

        return ['ip' => $ip];
    }

    /**
     * Stop OpenVPN process
     *
     * @param $id
     * @return array|bool
     */
    public static function stop($id)
    {
        Socket::send('disconnect', trans('dashboard.openvpn_disconnecting'));

        $connection = Connection::where('group_id', $id)->active()->first();

        if(is_null($connection)) {
            return ['error' => ['connection_failed' => [trans('dashboard.connection_failed')]]];
        }

        if($connection->adapter->connected) {

            // Issue disconnect command
            $pfsense = new PfSense();
            $pfsense->disconnectOpenVPN($connection->directives['ids']['vpnclient_id']);

            Socket::send('disconnect', trans('dashboard.verifying_disconnect'));

            sleep(15);

            // Verify that it's not running anymore
            if(System::isOpenVpnRunning($connection->directives['ids']['vpnclient_id'])) {
                return ['error' => ['disconnect_failed' => [trans('dashboard.disconnect_failed')]]];
            }
        }

        Socket::send('disconnect', trans('dashboard.final_system_changes'));

        // Update connection & group
        $connection->update(['disconnected_at' => Carbon::now()]);
        $connection->group()->update(['interface_id' => null, 'killswitch' => false]);

        // Generate a new config.xml file
        Config::generate();
        Config::flushFirewall();

        // Delete traffic history
        Traffic::where('group_id', $id)->delete();

        return true;
    }


    /**
     * Get the OpenVPN log file for a specific adapter
     *
     * @param $adapter_id
     * @return bool|string
     */
    public static function getLog($adapter_id)
    {
        if (App::environment() == 'local') {
            $file = storage_path('openvpn.log');
        } else {
            $file = '/tmp/openvpn-' . $adapter_id . '.log';
        }

        if(file_exists($file)) {
            return nl2br(file_get_contents($file));
        }

        return false;
    }

    /**
     * Parses the OpenVPN log file in case of an error so we can return a readable error message
     *
     * @param $adapter_id
     */
    public static function parseError($adapter_id)
    {
        // Get the OpenVPN log
        $log = self::getLog($adapter_id);

        if(!$log) {
            // For some obscure reason we couldn't get the log file. Let's just return a generic error message
            return trans('dashboard.failed_to_fetch_log');
        }

        // Explode line breaks so we can read the log line by line
        $explode = explode("<br>", $log);

        // Verify that we could get an array of linebreaks
        if($explode) {

            // Loop through linebreaks
            foreach($explode as $line) {

                // Check for AUTH fail
                if(String::in('auth_fail', $line)) {

                    // Fetch OVPN credentials from database
                    $ovpn = Ovpn::first();

                    // Login via API to check connected devices
                    $api = new Api();
                    $result = $api->login($ovpn->username, $ovpn->password);

                    if($result) {
                        if($result->devices >= 4) {
                            return trans('dashboard.too_many_devices');
                        }
                    } else {
                        return trans('dashboard.openvpn_failed');
                    }
                }

                // Check for TLS key negotation
                if(String::in('TLS key negotiation failed to occur', $line)) {
                    $ovpn = Ovpn::first();
                    
                    if($ovpn->protocol == 'udp') {
                        return trans('dashboard.failed_key_negotation_udp');
                    } else {
                        return trans('dashboard.failed_key_negotation');
                    }
                }


            }
        }
    }

}
