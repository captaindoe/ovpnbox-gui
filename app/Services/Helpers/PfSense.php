<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-03-12
 * Time: 13:39
 */

namespace App\Services\Helpers;


use App\Services\Network\Socket;
use App\Services\Shell\System;
use App\User;
use Exception;
use Goutte\Client;
use Log;
use Symfony\Component\DomCrawler\Crawler;

class PfSense
{

    protected $client;

    /**
     * PfSense constructor.
     */
    public function __construct()
    {
        // Create client
        $this->client = new Client();
        $this->client->getClient()->setDefaultOption('config/curl/'.CURLOPT_SSL_VERIFYHOST, FALSE);
        $this->client->getClient()->setDefaultOption('config/curl/'.CURLOPT_SSL_VERIFYPEER, FALSE);

        // Login
        $this->login();
    }

    /**
     * Login to pfSense GUI
     *
     * @return bool
     */
    protected function login()
    {
        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/index.php');
            $csrf = $crawler->filter('input[type=hidden]')->each(function ($node) {
                return $node->attr('value');
            });

            $form = $crawler->selectButton('Login')->form();
            $this->client->submit($form, array('usernamefld' => 'bot', 'passwordfld' => User::find(1)->value('bot_password'), '__csrf_magic' => $csrf[0]));

            return true;
        } catch(Exception $ex) {
            Log::error('Misslyckades logga in till pfSense', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Set a static IP address for a specific device
     *
     * @param $mac
     * @param $name
     * @param $ip
     * @return bool
     */
    public function setIP($mac, $name, $ip)
    {
        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp_edit.php?if=lan&mac=' . strtolower($mac) . '&hostname=' . String::fixHostname($name));
            $form = $crawler->selectButton('Save')->form();
            $form['ipaddr'] = $ip;

            // submit that form
            $mapping = $this->client->submit($form);

            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp_edit.php');
            $form = $crawler->selectButton('Apply changes')->form();
            $apply = $this->client->submit($form);

            return true;
        } catch(Exception $ex) {
            Log::error('Misslyckades lägga till IP-adress', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Get all current static IP addresses assigned
     *
     * @return array
     */
    public function getStaticIps()
    {
        $staticIps = [];

        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp.php');
            $paths = $crawler->filterXPath("//table[@summary='static mappings']//tr");

            foreach($paths as $x => $entry) {

                if($x == 0 || $x == 1) {
                    continue;
                }

                $value = $entry->nodeValue;

                if (preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $value, $ip_match)) {
                   $staticIps[] = $ip_match[0];
                }
            }

            return $staticIps;

        } catch(Exception $ex) {
            Log::error('Misslyckades hitta lediga statiska adresser', ['ex' => (array)$ex]);
            return [];
        }
    }

    public function renameDevice($ip, $name)
    {
        $device_id = String::staticIpToDeviceId($ip);

        if(is_null($device_id)) {
            return false;
        }

        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp_edit.php?if=lan&id=' . $device_id);
            $form = $crawler->selectButton('Save')->form();
            $form['hostname'] = String::fixHostname($name);

            // submit that form
            $mapping = $this->client->submit($form);

            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp.php?if=lan');
            $form = $crawler->selectButton('Apply changes')->form();
            $apply = $this->client->submit($form);

            return true;

        } catch(Exception $ex) {
            Log::error('Misslyckades döpa om enhet', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Removed static IP assignment for device
     *
     * @param $mac
     * @return bool
     */
    public function removeIP($mac)
    {
        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp.php');
            $paths = $crawler->filterXPath("//table[@summary='static mappings']//tr");

            $y = 0;
            if(is_array($paths)) {
                foreach($paths as $x => $entry) {

                    if($x == 0 || $x == 1) {
                        continue;
                    }

                    $value = $entry->nodeValue;
                    $replaced = str_replace(["\t", '"'], ['', ''], $value);
                    $explode = explode("\n", $replaced);
                    $row = [];
                    foreach($explode as $node) {
                        $trim = trim($node);

                        if(!empty($node)) {
                            $row[] = $trim;
                        }
                    }

                    if(count($row) <= 1) {
                        continue;
                    }

                    if($mac == $row[0]) {
                        $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp.php?if=lan&act=del&id=' . ($y));
                        $crawler =  $this->client->request('GET', 'https://' . System::getLanIp() . ':444/services_dhcp.php?if=lan');
                        $form = $crawler->selectButton('Apply changes')->form();
                        $this->client->submit($form);
                        return true;
                    }

                    $y++;
                }
            }

            return false;
        } catch(Exception $ex) {
            Log::error('Misslyckades ta bort IP-adress', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Disable WiFi network
     *
     * @return bool
     */
    public function disableWifi()
    {
        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/interfaces.php?if=opt1');
            $form = $crawler->selectButton('Save')->form();
            $form['enable']->untick();
            $this->client->submit($form);

            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/interfaces.php?if=opt1');
            $form = $crawler->selectButton('Apply changes')->form();
            $this->client->submit($form);

            return true;
        } catch(Exception $ex) {
            Log::error('Misslyckades inaktivera wifi', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Set WiFi network settings
     *
     * @param $ssid
     * @param $password
     * @param $frequency
     * @return bool
     */
    public function setWireless($ssid, $password, $frequency)
    {
        try {
            $wifi = System::getWifiData();

            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/interfaces.php?if=opt1');
            $form = $crawler->selectButton('Save')->form();
            $form['ssid'] = $ssid;
            $form['enable']->tick();
            $form['wpa_enable']->tick();
            $form['passphrase'] = $password;

            if($frequency == 2) {
                $form['channel']->select(1);
                $form['standard']->select('11ng');
                $channel = 1;
            } else {
                $form['channel']->select(64);
                $form['standard']->select('11na');
                $channel = 64;
            }

            if($wifi->ssid != $ssid) {
                Socket::send('wireless', trans('wireless.wireless_devices_disconnected'));
            } else {
                Socket::send('wireless', trans('wireless.update_soon_complete'));
            }

            $this->client->submit($form);

            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/interfaces.php?if=opt1');
            $form = $crawler->selectButton('Apply changes')->form();
            $this->client->submit($form);

            if($wifi->channel != $channel) {
                Socket::send('wireless', trans('wireless.rebooting'));
                shell_exec('reboot');
            }

            return true;
        } catch(Exception $ex) {
            Log::error('Failed to change wifi', ['error' => (array)$ex]);
            return false;
        }
    }

    /**
     * Connect to OpenVPN
     *
     * @param $id
     * @param $ip
     * @param $ports
     * @param $credentials
     * @param $protocol
     * @return bool
     */
    public function setOpenVPN($id, $ip, $ports, $credentials, $protocol)
    {
        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/vpn_openvpn_client.php?act=edit&id=' . $id);
            $form = $crawler->selectButton('Save')->form();
            $form['disable']->untick();
            $form['server_addr'] = $ip;
            $form['auth_user'] = $credentials->username;
            $form['auth_pass'] = $credentials->password;
            $form['route_no_pull']->untick();
            $form['route_no_exec']->tick();

            if ($protocol === "tcp") {
              $form['protocol']->select("TCP");
              $form['server_port'] = 443;
              $form['custom_options'] = 'auth-retry nointeract;
remote-cert-tls server;
reneg-sec 432000;
log /tmp/openvpn-' . $id . '.log;
tls-auth /usr/local/etc/ovpnse/tls.key 1;';
            } else {
              $form['protocol']->select("UDP");
              $form['server_port'] = $ports[0];
              $form['custom_options'] = 'remote ' . $ip . ' ' . $ports[1] . ';
remote-random;
auth-retry nointeract;
remote-cert-tls server;
reneg-sec 432000;
mute-replay-warnings;
replay-window 256;
log /tmp/openvpn-' . $id . '.log;
tls-auth /usr/local/etc/ovpnse/tls.key 1;';
            }

            $this->client->submit($form);

            return true;
        } catch(Exception $ex) {
            Log::error('Misslyckades spara inställningar för OpenVPN', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Disconnect from OpenVPN
     *
     * @param $client_id
     * @return bool
     */
    public function disconnectOpenVPN($client_id)
    {
        try {
            $this->client->request('GET', 'https://' . System::getLanIp() . ':444/status_services.php?mode=stopservice&service=openvpn&vpnmode=client&id=' . $client_id);
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/vpn_openvpn_client.php?act=edit&id=' . ($client_id-1));
            $form = $crawler->selectButton('Save')->form();
            $form['disable']->tick();
            $this->client->submit($form);
            return true;
        } catch(Exception $ex) {
            Log::error('Misslyckades koppla ner OpenVPN', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Reload firewall rules
     *
     * @return bool
     */
    public function reloadFirewall()
    {
        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/status_filter_reload.php');
            $form = $crawler->selectButton('Reload Filter')->form();
            $this->client->submit($form);

            return true;
        } catch(Exception $ex) {
            Log::error('Misslyckades ladda om brandvägg', ['ex' => (array)$ex]);
            return false;
        }
    }

    /**
     * Fetch OpenVPN status for a specific connection
     *
     * @param $client_id
     * @return mixed|string
     */
    public function getOpenVPNStatus($client_id)
    {
        try {
            $crawler = $this->client->request('GET', 'https://' . System::getLanIp() . ':444/status_openvpn.php');
            $paths = $crawler->filterXPath("//table[@class='tabcont sortable']//tr");

            foreach($paths as $x => $entry) {

                if($x == 0) {
                    continue;
                }

                $value = $entry->nodeValue;
                $replaced = str_replace(["\t", '"'], ['', ''], $value);
                $explode = explode("\n", $replaced);
                $row = [];
                foreach($explode as $node) {
                    $trim = trim($node);
                    if(!empty($node)) {
                        $row[] = $trim;
                    }
                }

                if(count($row) <= 1) {
                    continue;
                }

                if($row[0] == "OVPN.se client " . $client_id . " UDP") {
                    return $row[1];
                }
            }

            return 'down';
        } catch(Exception $ex) {
            Log::error('Misslyckades få OpenVPN status', ['ex' => (array)$ex]);
            return 'down';
        }
    }

    /**
     * Update pfSense version
     *
     * @return bool
     */
    public function updatePfSense()
    {
        try {
            $crawler =  $this->client->request('GET', 'https://' . System::getLanIp() . ':444/system_firmware_check.php');
            $form = $crawler->selectButton('Invoke Auto Upgrade')->form();
            $this->client->submit($form);
            return true;
        } catch(Exception $ex) {
            Log::error('Misslyckades uppdatera pfsense', ['ex' => (array)$ex]);
        }

        return false;
    }
}
