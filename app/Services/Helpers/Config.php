<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-03-28
 * Time: 09:46
 */

namespace App\Services\Helpers;


use App\Group;
use App\Services\Shell\System;
use SimpleXMLElement;

class Config
{

    /**
     * Generates OVPNs entries to the config file.
     */
    public static function generate()
    {

        // Load the pfsense configuration file
        $path = System::getPfsenseConfiguration();

        $xml = new SimpleXMLElement(
            file_get_contents($path)
        );

        if(isset($xml->ovpnse)) {
            unset($xml->ovpnse);
        }

        $ovpn = $xml->addChild('ovpnse');
        $ovpngroups = $ovpn->addChild('ovpnse-groups');

        // Get all groups
        $groups = Group::visible()->get();

        // Verify results
        if(!$groups->isEmpty()) {

            // Loop through all groups
            foreach($groups as $group) {

                // Verify group has devices
                if(!$group->devices->isEmpty()) {

                    $ovpngroup = $ovpngroups->addChild('ovpnse-group');

                    // Check if connected
                    if(is_null($group->interface_id)) {

                        // Not connected
                        $ovpngroup->addChild('ovpnse-interface', System::getWanInterface());
                    } else {

                        // Connected
                        $ovpngroup->addChild('ovpnse-interface', $group->adapter->name);
                    }

                    $ovpngroup->addChild('ovpnse-killswitch', $group->killswitch);
                    $ovpnclients = $ovpngroup->addChild('ovpnse-clients');

                    // Loop through devices
                    foreach($group->devices as $device) {

                        $ovpnclient = $ovpnclients->addChild('ovpnse-client');
                        $ovpnclient->addChild('ovpnse-host', $device->ip);
                        $ovpnclient->addChild('ovpnse-temp_bypass', $device->bypass);
                        $ovpnports = $ovpnclient->addChild('ovpnse-ports');

                        // Verify group has ports
                        if(!$device->ports->isEmpty()) {

                            // Loop through ports
                            foreach($device->ports as $port) {
                                $ovpnport = $ovpnports->addChild('ovpnse-port');
                                $ovpnport->addChild('ovpnse-protocol', $port->config_type);
                                $ovpnport->addChild('ovpnse-start', $port->start);
                                $ovpnport->addChild('ovpnse-stop', $port->end);
                            }
                        }
                    }
                }
            }
        }

        // Check if the unsorted group is connected
        $unsorted = Group::connected()->hidden()->first();

        // Verify results
        if(!is_null($unsorted)) {
            $ovpnunsorted = $ovpn->addChild('ovpnse-unsorted');
            $ovpnunsorted->addChild('ovpnse-interface', $unsorted->adapter->name);
            $ovpnunsorted->addChild('ovpnse-killswitch', $unsorted->killswitch);
        }

        file_put_contents($path, $xml->asXML(), LOCK_EX);

        $temp = '/tmp/config.cache';
        if(file_exists($temp)) {
            unlink($temp);
        }

        return true;
    }

    public static function flushFirewall()
    {
        $pfsense = new PfSense();
        $pfsense->reloadFirewall();
    }
}