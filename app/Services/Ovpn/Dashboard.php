<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-03-23
 * Time: 18:55
 */

namespace App\Services\Ovpn;


use App\Services\Network\Socket;
use App\Services\Shell\Traceroute;
use Illuminate\Support\Facades\Redis;

class Dashboard
{

    public static function getIpForConnection($type, $manual, $addon)
    {
        $traceroute = new Traceroute();
        $api = new Api();

        if($addon == 'public-ipv4') {
            $server = $api->getBestServersInDatacenter('sthlm', 1);
            $ip = $server->ip;
        } else {
            if($type == 'auto') {

                Socket::send('connect', trans('dashboard.best_route'));

                // Take best datacenter
                $datacenters = $traceroute->getBestRoute();
                $dc = $datacenters['best']['slug'];

                Socket::send('connect', trans('dashboard.datacenter_found'));
                $server = $api->getBestServersInDatacenter($dc, 1);

                if(!$server) {
                    return false;
                }

                $ip = $server->ip;


            } else if($type == 'manual') {
                $ip = $manual;
            } else {

                Socket::send('connect', trans('dashboard.checking_route_in') . ' ' . trans('dashboard.' . $type) . ' ' . trans('dashboard.best_route_to'));

                // Take best datacenter in specific country
                $datacenters = $traceroute->getBestRoute();
                foreach ($datacenters['datacenters'] as $key => $datacenter) {
                    if ($datacenter->country != $type) {
                        unset($datacenters['datacenters'][$key]);
                    }
                }

                if (count($datacenters['datacenters']) > 1) {
                    $sort = [];
                    foreach ($datacenters['datacenters'] as $key => $row) {
                        $sort[$key] = $row->hops;
                    }
                    array_multisort($sort, SORT_ASC, $datacenters['datacenters']);
                } else {
                    sort($datacenters['datacenters']);
                }

                $dc = $datacenters['datacenters'][0]->slug;

                Socket::send('connect', trans('dashboard.datacenter_found'));

                $server = $api->getBestServersInDatacenter($dc, 1);

                if(!$server) {
                    return false;
                }

                $ip = $server->ip;
            }
        }

        Socket::send('connect', trans('dashboard.starting_connection'));

        return $ip;
    }

    public static function getPortsForAddon($addon)
    {
        $config_types = [
            'normal' => [
                    1194,
                    1195
            ],
            'filtering' => [
                1198,
                1199
            ],
            'public-ipv4' => [
                1196,
                1197
            ],
            'multihop' => [
                1201,
                1202
            ]
        ];

        if(!isset($config_types[$addon])) {
            return false;
        }

        return $config_types[$addon];
    }
}