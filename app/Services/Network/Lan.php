<?php


namespace App\Services\Network;


use App;
use App\Device;
use App\Group;
use App\Services\Helpers\PfSense;
use App\Services\Shell\System;

class Lan
{

    /**
     * Removes static IP assignment in pfSense and from device database
     *
     * @param $mac
     */
    static function destroyDevice($mac)
    {
        $existing = Device::where('mac', $mac)->first();

        if(!is_null($existing)) {

            // Delete IP assignment in pfSense
            $pfsense = new PfSense();
            $pfsense->removeIP($mac);

            // Delete IP address from database
            $existing->ports()->delete();
            $existing->delete();
        }

        return true;
    }

    /**
     * @param $groups Group[]
     * @return array
     */
    static function getMacAddresses($groups)
    {
        $macs = [];
        if(!is_null($groups)) {
            foreach($groups as $group) {
                if(!$group->devices->isEmpty()) {
                    foreach ($group->devices as $device) {
                        $macs[] = $device->mac;
                    }
                }
            }
        }

        return $macs;
    }

    /**
     * Returns unsorted devices
     *
     * @return array|bool
     */
    static function getUnsortedDevices($macs)
    {
        if (App::environment('local')) {
            $unsorted = [['hostname' =>  'sdg', 'ip' => '10.220.0.28', 'mac' => '62:d2:je:00:2d:70'], ['hostname' =>  'sfhjdjd', 'ip' => '10.220.0.125', 'mac' => '62:d2:ee:00:2d:70'], ['hostname' =>  'shdfjdj', 'ip' => '10.220.0.165', 'mac' => '22:d2:je:00:2d:70'], ['hostname' =>  'sfhdjd', 'ip' => '10.220.0.185', 'mac' => '96:2f:d4:7b:26:0c'], ['hostname' =>  'sdg', 'ip' => '10.220.0.30', 'mac' => '72:05:26:e8:62:26']];
        } else {
            $unsorted = App\Services\Shell\DhcpLeases::get();
        }

        if(!empty($unsorted)) {
            foreach($unsorted as $key => $device) {
                if(isset($device['mac'])) {
                    if(in_array($device['mac'], $macs)) {
                        unset($unsorted[$key]);
                    }
                }
            }

            if(!empty($unsorted)) {
                sort($unsorted);
            }
        }

        return $unsorted;
    }

    /**
     * Fetch an available device name
     *
     * @param $name
     * @return string
     */
    static function getAvailableDeviceName($name)
    {
        // Remove whitespaces
        $name = trim($name);

        // Check if occupied
        $already_named = Device::where('name', $name)->get();

        if($already_named->count() > 0) {
            return $name . ' ' . ($already_named->count()+1);
        }

        return $name;
    }

    /**
     * Fetch an available IP to assign device
     *
     * @return bool|string
     */
    static function getAvailableIp()
    {
        $pfsense = new PfSense();
        $staticIps = $pfsense->getStaticIps();

        // First available IP is x.x.x.2 and maximum available IP is x.x.x.100
        $min_ip = 2;
        $max_ip = 100;
        $full = false;
        $ip = substr(System::getLanIp(), 0, -1);

        // Check available IP address
        while($min_ip <= $max_ip) {

            // Combine subnet and number to test
            $full = $ip . $min_ip;

            // Check if it's already used
            if (!in_array($full, $staticIps)) {
                break;
            }

            // Increase number to test
            $min_ip++;
        }

        return $full;
    }
}