<?php

namespace App\Services\Network;
use App\Interfaces;

/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-02-25
 * Time: 09:48
 */
class Adapter
{

    /**
     * Get a list of all network adapters on the system
     *
     * @return array|mixed
     */
    public function get()
    {
        $ifconfig = new \Datasift\IfconfigParser\Parser\Darwin();
        $ifconfigOutput = shell_exec('/sbin/ifconfig');

        return $ifconfig->parse($ifconfigOutput);
    }

    /**
     * Get available OpenVPN adapter to use for connection
     *
     * @param $group_id
     * @return bool|string
     */
    public function getAvailableOpenVPN($group_id)
    {
        if($group_id == 1) {
            return 'ovpnse1';
        }

        $adapters = $this->get();
        $selected = Interfaces::where('type', 'openvpn')->get();
        $openvpn = [];

        foreach($selected as $entry)
        {
            $openvpn[] = $entry->name;
        }

        foreach($adapters as $adapter) {
            if($adapter['interface'] == "ovpnse1") {
                continue;
            }

            if(in_array($adapter['interface'], $openvpn)) {
                if(!isset($adapter['ip_address'])) {
                    return $adapter['interface'];
                }
            }
        }

        return false;
    }

    /**
     * Get external IP address for a specific interface
     *
     * @param $interface
     * @return array|bool
     */
    public function getStaticIP($interface)
    {
        $time_start = microtime(true);
        $data = json_decode(shell_exec('curl --interface ' . escapeshellarg($interface) . ' https://www.ovpn.se/v1/api/client/ptr'));
        $time_end = microtime(true);

        \Log::debug('Debug the time it takes to get static IP', ['interface' => $interface, 'time' => ($time_end-$time_start)]);

        if(!is_null($data)) {
            if($data->status) {
                return [
                    'ip' => $data->ip,
                    'ptr' => $data->ptr
                ];
            }
        }

        return false;
    }
}