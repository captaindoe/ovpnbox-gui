<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-03-30
 * Time: 16:05
 */

namespace App\Services\Network;


use Illuminate\Support\Facades\Redis;

class Socket
{

    /**
     * Push a message to the socket through Redis
     *
     * @param $event
     * @param $message
     */
    public static function send($event, $message)
    {
        Redis::publish('ovpn', json_encode(['event' => $event, 'data' => ['message' => $message]]));
    }
}