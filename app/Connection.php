<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    protected $fillable = ['group_id', 'interface_id', 'directives', 'disconnected_at'];
    protected $casts = [
        'directives' => 'array',
    ];
    /*
     * Each connection belongs to an interface that it uses
     */
    public function adapter()
    {
        return $this->belongsTo('App\Interfaces', 'interface_id');
    }

    /**
     * Each connection belongs to a group that it uses
     */
    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }

    /**
     * Scope a query to only include groups connected to OVPN
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->whereNull('disconnected_at');
    }
}
