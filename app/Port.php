<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Port extends Model
{
    protected $fillable = ['device_id', 'start', 'end', 'type'];

    /**
     * Get the phone record associated with the user.
     */
    public function device()
    {
        return $this->belongsTo('App\Device');
    }

    public function getRangeAttribute()
    {
        if($this->attributes['start'] ==  $this->attributes['end']) {
            return $this->attributes['start'];
        }

        return $this->attributes['start'] . ' - ' . $this->attributes['end'];
    }

    public function getTypeAttribute($value)
    {
        if($value == '1') {
            return 'TCP';
        } elseif($value == '2') {
            return 'UDP';
        } else {
            return 'TCP/UDP';
        }
    }

    public function getConfigTypeAttribute()
    {
        if($this->attributes['type'] == '1') {
            return 'tcp';
        } elseif($this->attributes['type'] == '2') {
            return 'udp';
        } else {
            return 'both';
        }
    }
}
