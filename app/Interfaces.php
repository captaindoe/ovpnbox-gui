<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interfaces extends Model
{
    protected $fillable = ['name', 'type'];

    /**
     * Each active interface has one group
     */
    public function group()
    {
        return $this->hasOne('App\Group');
    }

    /*
     * Each active interface has one connection
     */
    public function connection()
    {
        return $this->hasOne('App\Connection');
    }

    public function getConnectedAttribute()
    {
        if(file_exists('/tmp/' . $this->attributes['name'] . 'up')) {
            return true;
        }

        return false;
    }
}
