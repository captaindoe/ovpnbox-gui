<?php

namespace App\Console\Commands;

use App\Ovpn;
use App\Services\Ovpn\Api;
use App\Services\Shell\System;
use App\User;
use Exception;
use Illuminate\Console\Command;
use Log;

class SendBoxPing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'box:ping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a ping to OVPN.se with information about software versions running';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $gui = System::getBoxCommit();

            $api = new Api();
            $api->sendPing([
                'username' => Ovpn::first()->username,
                'firmware' => System::getBoxFirmwareVersion(),
                'gui' => $gui['short'] . '/' . $gui['branch'],
                'uptime' => System::getSystemUptime()
            ]);
        } catch(Exception $ex) {
            Log::error('Failed to send ping', ['error' => (array)$ex]);
        }
    }
}
