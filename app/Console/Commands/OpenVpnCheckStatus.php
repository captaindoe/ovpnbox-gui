<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Connection;
use App\Interfaces;
use App\Services\Network\Adapter;
use App\Services\Helpers\PfSense;
use App\Services\Helpers\String;
use Log;

/**
 * Gets all current interfaces, filters out non-ovpn ones,
 * checks the Connection model to see if they're disconnected
 * and if so closes them.
 */
class OpenVpnCheckStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openvpn:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifies only correct interfaces are connected to OVPN';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $adapter = new Adapter();
        $pfsense = new PfSense();

        $interfaces = array_filter($adapter->get(), function($arr) {
            return str_contains($arr['interface'], "ovpnse");
        });

        if (empty($interfaces)) {
            $this->info("No interfaces found.");
            return false;
        }

        foreach ($interfaces as $interface) {

            $interface_id = Interfaces::where("name", $interface['interface'])->value("id");

            if (is_null($interface_id)) {
                Log::error('No interface_id found', ["interface" => $interface]);
                continue;
            }

            $connection = Connection::active()->where("interface_id", $interface_id)->first();

            if (is_null($connection) && isset($interface['ip_address'])) {

                $client_id = String::adapterToVpnClientId($interface['interface']);
                $result = $pfsense->disconnectOpenVPN($client_id);

                if ($result) {

                    $this->info("Successfully disconnected " . $interface['interface']);

                } else {

                    $this->error("Failed to disconnect " . $interface['interface']);
                    Log::error('Failed to disconnect unsynced interface', ["interface" => $interface, 'connection' => (array)$connection]);

                }

            } else {

                $this->info("Interface " . $interface['interface'] . " is synced correctly.");

            }
        }
        return true;
    }
}
