<?php

namespace App;

use App\Services\Shell\System;
use Illuminate\Database\Eloquent\Model;
use Log;

class Group extends Model
{
    //
    protected $fillable = ['name', 'interface_id', 'killswitch', 'hidden'];
    protected $casts = [
        'killswitch' => 'boolean',
        'hidden' => 'boolean',
    ];

    /**
     * Each group has many devices
     */
    public function devices()
    {
        return $this->hasMany('App\Device', 'group_id');
    }

    /**
     * Each group belongs to an interface
     */
    public function adapter()
    {
        return $this->belongsTo('App\Interfaces', 'interface_id');
    }

    /**
     * Each group has many connections
     */
    public function connection()
    {
        return $this->hasMany('App\Connection', 'group_id');
    }

    /**
     * Each group has many traffic records
     */
    public function traffic()
    {
        return $this->hasMany('App\Traffic', 'group_id');
    }

    /**
     * Get traffic for group
     */
    public function getInterfaceTraffic()
    {
        $adapter = Interfaces::where('id', $this->attributes['interface_id'])->first();
        if(is_null($adapter)) {
            Log::debug('Interface was not found', ['group' => (array)$this->attributes]);
            return ['download' => 0, 'upload' => 0];
        }
        return System::getTotalTraffic($adapter->name);
    }

    /**
     * Get download traffic for group
     */
    public function getTrafficDownloadAttribute()
    {
        $traffic = $this->getInterfaceTraffic();
        return number_format((float)$traffic['download'], 2, '.', '');
    }

    /**
     * Get upload traffic for group
     */
    public function getTrafficUploadAttribute()
    {
        $traffic = $this->getInterfaceTraffic();
        return number_format((float)$traffic['upload'], 2, '.', '');
    }

    public function getConnectionDetailsAttribute()
    {
        if(!$this->adapter->connected) {
            return null;
        }

        $connection = $this->connection()->active()->first();

        if(is_null($connection)) {
            return null;
        }

        return [
            'server' => $connection['event']['server'],
            'addon' => $connection['event']['addon'],
            'adapter' => $this->adapter->name,
            'traffic' =>
                [
                    'download' => $this->traffic_download,
                    'upload' => $this->traffic_upload
                ]
        ];
    }

    /**
     * Scope a query to only include groups connected to OVPN
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDisconnected($query)
    {
        return $query->whereNull('interface_id');
    }

    /**
     * Scope a query to only include groups that aren't visible
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible($query)
    {
        return $query->where('hidden', false);
    }

    /**
     * Scope a query to only include groups that are hidden
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHidden($query)
    {
        return $query->where('hidden', true);
    }

    /**
     * Scope a query to only include groups that are not connected to OVPN
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeConnected($query)
    {
        return $query->whereNotNull('interface_id');
    }
}
