<?php

namespace App;

use App\Services\Network\Ping;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //
    protected $fillable = ['mac', 'ip', 'group_id', 'name', 'bypass'];
    protected $casts = [
        'bypass' => 'boolean',
    ];

    /*
     * Each device belongs to a group
     */
    public function group() {
        return $this->belongsTo('App\Group');
    }

    /*
     * Each devices has multiple ports
     */
    public function ports()
    {
        return $this->hasMany('App\Port', 'device_id');
    }

    /**
     * IP mutator that converts long to an IP address
     *
     * @param $value
     * @return string
     */
    public function getIpAttribute($value)
    {
        return long2ip($value);
    }

    /**
     * Name mutator that returns unknown if no name is applied
     *
     * @param $value
     * @return mixed
     */
    public function getNameAttribute($value)
    {
        if(is_null($value)) {
            return trans('devices.unknown');
        }

        return $value;
    }

    /**
     * Mutator to convert IP address to long
     *
     * @param $value
     */
    public function setIpAttribute($value)
    {
        $this->attributes['ip'] = sprintf("%u", ip2long($value));
    }

    /**
     * Sends a ping request to device to see if it's connected to the network
     *
     * @return bool
     */
    public function isUp()
    {
        return Ping::get($this->attributes['ip']);
    }

    /**
     * Scope a query to sort devices by ascending IP addresses
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAscending($query)
    {
        return $query->orderBy('ip', 'asc');
    }


}
