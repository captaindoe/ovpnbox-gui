<?php

namespace App\Http\Controllers;

use App;
use App\Device;
use App\Group;
use App\Port;
use App\Services\Helpers\Config;
use App\Services\Helpers\PfSense;
use App\Services\Helpers\String;
use App\Services\Network\Lan;
use App\Services\Network\StaticIp;
use App\Services\Shell\System;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Response;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'devices', 'title' => trans('general.devices')];

        // Fetch groups and devices from databases
        $groups = Group::visible()->get();
        $hidden = Group::connected()->hidden()->first();
        $devices = Device::all();

        // Get MAC addresses and unsorted devices
        $macs = Lan::getMacAddresses($groups);
        $unsorted = Lan::getUnsortedDevices($macs);

        // Get example LAN IP address
        $lan = System::getExampleLanIp();

        return view('pages.devices', compact('meta', 'groups', 'unsorted', 'hidden', 'devices', 'lan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'mac' => 'required',
            'group' => 'required',
        ]);

        $mac = trim($request->get('mac'));
        $name = $request->get('name');
        $group = $request->get('group');
        $ipaddr = $request->get('ipaddr');

        // Validate MAC address
        if(!String::getValidMac($mac)) {
            return Response::json(['group' => [trans('devices.cant_find_mac')]], 422);
        }

        // Handle group model based on input
        if($group == 'create') {
            $model = Group::create(['name' => trans('devices.group') . ' ' . (Group::all()->count())]);
        } else if($group == 'choose') {
            return Response::json(['group' => [trans('devices.choose_existing_or_not')]], 422);
        } else {
            $model = Group::findOrFail($group);
        }

        // Destroy device if it already exists
        Lan::destroyDevice($mac);
        
        // Check if specific IP address was entered
        if(!is_null($ipaddr)) {

            // Validate IP address
            if (!filter_var($ipaddr, FILTER_VALIDATE_IP)) {
                return Response::json(['group' => [trans('devices.invalid_ip')]], 422);
            }

            $ip = $ipaddr;
        } else {
            $ip = Lan::getAvailableIp();
        }

        // Get available device name
        $name = Lan::getAvailableDeviceName($name);

        // Add device to database
        $create = Device::create([
            'name' => $name,
            'mac' => $mac,
            'ip' => $ip,
            'group_id' => $model->id
        ]);

        // Add static assignment in pfSense
        $pfsense = new PfSense();
        $result = $pfsense->setIP($mac, $name, $ip);

        // Verify results
        if(!$result) {
            $pfsense->removeIP($mac);
            $create->delete();
            return Response::json(['failed' => [trans('devices.failed_assign_static')]], 422);
        }

        // Flush database
        Config::generate();
        Config::flushFirewall();

        return Response::json([
            'id' => $create->id,
            'group' => [
                'id' => $model->id,
                'title' => $model->name
            ],
            'group_array' => $model->toArray(),
            'device' => $name,
            'ip' => $ip,
            'mac' => $mac
        ], 200);
    }

    public function getStatus($id)
    {
        $device = Device::find($id);

        if($device->isUp()) {
            return Response::json(['status' => true], 200);
        } else {
            return Response::json(['status' => false], 200);
        }
    }

    /*
     * Add a device to bypass status
     */
    public function storeBypass($id)
    {
        Device::find($id)->update(['bypass' => true]);

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    /*
     * Remove a device from bypass status.
     */
    public function destroyBypass(Request $request, $id)
    {
        Device::find($id)->update(['bypass' => false]);

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    /*
     * Move device to another group
     */
    public function moveDevice(Request $request, $id)
    {
        $this->validate($request, [
            'group' => 'required|exists:groups,id',
        ]);

        Device::find($id)->update(['group_id' => $request->get('group')]);

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    /**
     * Update the specified device in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $name = Lan::getAvailableDeviceName($request->get('name'));

        $device = Device::find($id);

        $pfsense = new PfSense();
        $renamePfsense = $pfsense->renameDevice($device->ip, $name);
        $renameOvpn = $device->update(['name' => $name]);

        if(!$renamePfsense) {
            Log::debug('Misslyckades döpa om i pfsense');
        }

        if(!$renameOvpn) {
            Log::debug('Misslyckades döpa om enhet');
        }

        if(!$renamePfsense || !$renameOvpn) {
            //Lan::destroyDevice($device->mac);
            return Response::json(['failed' => [trans('devices.failed_update_device_name')]], 422);
        }

        return Response::json(['status' => true, 'name' => $name], 200);
    }

    /**
     * Remove the specified device from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find device
        $device = Device::find($id);

        $pfsense = new PfSense();
        $pfsense->removeIP($device->mac);

        $device->delete();

        Port::where('device_id', $id)->delete();

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);

    }
}
