<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Ovpn\Api;
use App\User;
use Carbon\Carbon;
use DB;
use Faker\Provider\Uuid;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Response;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
    protected $redirectTo = '/';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function postEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|exists:users,email'
        ]);

        $collection = DB::table('password_resets')->where('email', $request->get('email'));
        $resets = $collection->first();

        if(!is_null($resets)) {
            $collection->delete();
        }

        $uuid = Uuid::uuid();

        $create = DB::table('password_resets')->insert([
            'email' => $request->get('email'),
            'token' => $uuid,
            'created_at' => Carbon::now()
        ]);

        if($create) {
            $api = new Api();
            $reset = $api->sendBoxReset([
                'email' => $request->get('email'),
                'token' => $uuid,
                'username' => User::where('email', $request->get('email'))->first()->name
            ]);

            if($reset) {
                return Response::json(['status' => true], 200);
            } else {
                return Response::json(['error' => ['Failed to send reset password email.']], 422);
            }
        }

        return Response::json(['error' => ['Failed to reset password']], 422);
    }
}
