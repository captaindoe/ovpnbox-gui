<?php

namespace App\Http\Controllers;

use App;
use App\Group;
use App\Interfaces;
use App\Ovpn;
use App\Services\Network\Adapter;
use App\Services\Network\Ping;
use App\Services\Ovpn\Api;
use App\Services\Shell\System;
use App\User;
use Artisan;
use Auth;
use DB;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Response;

class SetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSetup($step = 1)
    {
        $adapter = new Adapter();
        $interfaces = $adapter->get();
        $wan = false;

        foreach($interfaces as $name) {
            if($name['interface'] == 'em0' || $name['interface'] == 're0') {
                $wan = $name['interface'];
                break;
            }
        }

        $meta = ['page' => 'setup', 'title' => trans('general.configure')];

        return view('pages.setup.step' . $step, compact('interfaces', 'meta', 'wan'));
    }

    public function postIntialSetup(Request $request)
    {
        if(!Ping::connectivity()) {
            return Response::json(['connectivity' => ['It seems you don\'t have an internet connection right now. ' .
                'Make sure the internet cable to WAN is properly inserted and try again. If this issue remains, ' .
                'please contact OVPNs support for troubleshoot assistance.']], 422);
        }

        $interfaces = Interfaces::all();
        $groups = Group::all();

        if($interfaces->isEmpty()) {
            $adapter = new Adapter();
            $adapters = $adapter->get();

            foreach($adapters as $name) {
                if($name['interface'] == 'em0' || $name['interface'] == 're0') {
                    Interfaces::create([
                        'type' => 'wan',
                        'name' => $name['interface']
                    ]);
                    break;
                }
            }

            Interfaces::create([
                'type' => 'lan',
                'name' => 'bridge0'
            ]);

            \App\Interfaces::create([
                'name' => 'ovpnse1',
                'type' => 'openvpn'
            ]);

            \App\Interfaces::create([
                'name' => 'ovpnse2',
                'type' => 'openvpn'
            ]);

            \App\Interfaces::create([
                'name' => 'ovpnse3',
                'type' => 'openvpn'
            ]);

            \App\Interfaces::create([
                'name' => 'ovpnse4',
                'type' => 'openvpn'
            ]);

            \App\Interfaces::create([
                'name' => 'ovpnse5',
                'type' => 'openvpn'
            ]);

            \App\Interfaces::create([
                'name' => 'ovpnse6',
                'type' => 'openvpn'
            ]);

            if(System::getWifiCard()) {
                \App\Interfaces::create([
                    'type' => 'wireless',
                    'name' => 'ath0_wlan0'
                ]);
            }

        }

        if($groups->isEmpty()) {
            Group::create([
                'name' => trans('setup.unsorted_devices'),
                'hidden' => true
            ]);
        }

        return Response::json(['status' => true]);
    }

    public function postOvpnLogin(Request $request)
    {

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $api = new Api();
        $result = $api->login($request->get('username'),$request->get('password'));

        if($result) {
            $find = Ovpn::where('username', $request->get('username'))->first();

            if(!is_null($find)) {
                $find->update([
                    'username' => $request->get('username'),
                    'password' => $request->get('password'),
                    'default' => $result->timestamp,
                    'filtering' => $result->addons->filtering->timestamp,
                    'multihop' => $result->addons->multihop->timestamp,
                    'public_ipv4' => $result->addons->{'public-ipv4'}->timestamp,
                    'ports' => $result->portforward
                ]);
            } else {
                Ovpn::create([
                    'username' => $request->get('username'),
                    'password' => $request->get('password'),
                    'default' => $result->timestamp,
                    'filtering' => $result->addons->filtering->timestamp,
                    'multihop' => $result->addons->multihop->timestamp,
                    'public_ipv4' => $result->addons->{'public-ipv4'}->timestamp,
                    'ports' => $result->portforward
                ]);
            }

            return Response::json(['status' => true]);
        }

        return Response::json(['credentials' => [trans('auth.failed')]], 422);
    }

    public function postSetupAccount(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'language' => 'required|in:sv,en'
        ]);

        $bot = System::getBotPasswordPath();

        if(!file_exists($bot)) {
            Log::critical('Bot lösenord har inte genererats');
            return Response::json(['bot_path' => [trans('setup.failed_generate')]], 422);
        }

        App::setLocale($request->get('language'));

        try {

            // Truncate users to make sure we're starting out clean.
            DB::table('users')->truncate();

            // Create user
            User::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => password_hash($request->get('password'), PASSWORD_BCRYPT, ["cost" => 12]),
                'bot_password' => file_get_contents($bot),
                'language' => $request->get('language')
            ]);

            // getting login info with new account.
            if (!Auth::attempt([
                'name' => $request->get('name'),
                'password' => $request->get('password')
            ], true)) {
                return Response::json(['credentials' => [trans('auth.failed')]], 422);
            }

            // Localize unsorted group
            Group::hidden()->update(['name' => trans('setup.unsorted_devices')]);

            // Send an initial box ping
            Artisan::call('box:ping');

        } catch(Exception $ex) {
            Log::error('Failed to create yser', ['error' => (array)$ex]);
            return Response::json(['credentials' => [trans('auth.failed')]], 422);
        }

        unlink($bot);
        return Response::json(['status' => true]);
    }

}
