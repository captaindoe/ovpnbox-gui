<?php

namespace App\Http\Controllers;

use App\Device;
use App\Ovpn;
use App\Port;
use App\Services\Helpers\Config;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class PortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'port', 'title' => trans('general.ports')];
        $ports = Port::orderBy('start', 'asc')->get();
        $devices = Device::all();
        $account = Ovpn::find(1);

        return view('pages.port', compact('meta', 'ports', 'devices', 'account'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'device' => 'required|exists:devices,id',
            'port_number_1' => 'required|integer|min:1|max:65535|unique:ports,start|unique:ports,end|not_in:81,444',
            'port_number_2' => 'required|integer|min:1|max:65535|unique:ports,start|unique:ports,end|not_in:81,444',
            'protocol' => 'required|in:1,2,3',
        ]);

        if($request->get('port_number_1') > $request->get('port_number_2')) {
            return Response::json(['port_range' => [trans('ports.port_one_lower')]], 422);
        }

        $ports = Port::all();

        if(!$ports->isEmpty()) {
            foreach($ports as $port) {

                if(
                    ($port->start <= $request->get('port_number_1') && $port->end >= $request->get('port_number_1')) ||
                    ($port->start <= $request->get('port_number_2') && $port->end >= $request->get('port_number_2'))
                ) {
                    return Response::json(['port_range' => [trans('ports.port_occupied')]], 422);
                }
            }
        }

        Port::create([
            'device_id' => $request->get('device'),
            'start' => $request->get('port_number_1'),
            'end' => $request->get('port_number_2'),
            'type' => $request->get('protocol')
        ]);

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $port = Port::find($id);
        $port->delete();

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }
}
