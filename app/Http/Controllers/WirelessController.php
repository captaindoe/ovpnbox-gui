<?php

namespace App\Http\Controllers;

use App\Services\Helpers\PfSense;
use App\Services\Network\Socket;
use App\Services\Shell\System;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class WirelessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'wireless', 'title' => trans('general.wireless')];

        $wifi = System::getWifiData();
        $enabled = System::isWifiEnabled();
        $range = System::getLanRange();

        return view('pages.wireless', compact('meta', 'wifi', 'range', 'enabled'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'wireless' => 'required|in:enable,disable',
            'network' => 'required_if:wireless,enable',
            'frequency' => 'required_if:wireless,enable|in:2,5',
            'password' => 'required_if:wireless,enable|min:6',
            'password_confirmation' => 'required_if:wireless,enable|same:password',
        ]);

        $pfsense = new PfSense();

        if($request->get('wireless') == 'disable') {
            Socket::send('wireless', trans('wireless.deactivating'));
            $pfsense->disableWifi();
        } else {
            Socket::send('wireless', trans('wireless.saving'));
            $pfsense->setWireless($request->get('network'), $request->get('password'), $request->get('frequency'));
        }

        return Response::json(['status' => true], 200);
    }
}
