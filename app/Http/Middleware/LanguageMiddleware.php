<?php

namespace App\Http\Middleware;

use App;
use Auth;
use Closure;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            if(isset(Auth::user()->language)) {
                App::setLocale(Auth::user()->language);
            }
        }

        return $next($request);
    }
}
