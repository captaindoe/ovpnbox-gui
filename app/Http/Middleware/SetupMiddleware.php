<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class SetupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::all()->count() > 0) {
            return redirect('/');
        }

        return $next($request);
    }
}
