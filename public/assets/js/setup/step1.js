$(function () {

    $("#setup").submit(function(event) {
        event.preventDefault();

        // Hide error message
        var error = $(".error"), button = $("#start");
        button.prop("disabled",true);
        error.addClass('hidden');

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.starting-configuration'));

        $.ajax({
            type: "POST",
            url:  "/setup/1",
            data: {
                _token: $('input[name=_token]').val()
            },
            async: true,
            cache: false,
            timeout:0,
            success: function () {
                window.location = '/setup/2';
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                button.prop("disabled", false);
                displayMessage('error', $.i18n('common.error'), err);
                button.html($.i18n('common.start-configuration'));
            }
        });
    });

});
