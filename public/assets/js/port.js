$(function () {

    /**
     * To open a port
     */
    $("#port_form").submit(function(event) {

        event.preventDefault();

        // Hide error message
        var device = $("#device"),
            port_number_1 = $("#port_number_1"),
            port_number_2 = $("#port_number_2"),
            type = $("#protocol"),
            table = $('.table'),
            info = $(".info"),
            portDisplay = false;

        hideMessage();
        displayMessage('info', $.i18n('port.opening'), $.i18n('port.working-on-open'));

        // Update button
        disableButtons('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        $.ajax({
            type: "POST",
            url:  "/port",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val(),
                device: device.val(),
                port_number_1: port_number_1.val(),
                port_number_2: port_number_2.val(),
                protocol: type.val()
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {

                if(port_number_1.val() == port_number_2.val()) {
                    portDisplay = port_number_1.val();
                } else {
                    portDisplay = port_number_1.val() + '-' + port_number_2.val();
                }

                $('#port-list').append(
                        '<tr>' +
                            '<td>' + device.find("option:selected").text() + '</td>' +
                            '<td>' + portDisplay + '</td>' +
                            '<td>' + type.find("option:selected").text() + '</td>' +
                            '<td><a href="javascript:void(0);" title="' + $.i18n('port.opened') + '"><i class="fa fa-check"></i></a></td></tr>');

                if(table.hasClass('hidden')) {
                    table.removeClass('hidden');
                    $(".ports-display").addClass('hidden');
                }

                hideMessage();
                enableButtons();
                port_number_1.val('');
                port_number_2.val('');
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                enableButtons();
            }
        });


    });

    // Form that gets executed when user tries to delete a port forward
    $("body").on('click', '.delete_port', function() {

        // Hide error message
        var  table = $('.table'), el = $(this);

        hideMessage();

        $.ajax({
            type: "DELETE",
            url:  "/port/" + el.data('portid'),
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
              _token: $('input[name=_token]').val()
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {

                el.parent().parent().remove();

                if($('tbody tr').length == 0) {
                    table.addClass('hidden');
                    $(".ports-display").removeClass('hidden');
                }

                hideMessage();
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
            }
        });
    });
});
