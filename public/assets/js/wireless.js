$(function () {

    // Event listener for changing connection type
    $("#wireless").change(function() {
        if($(this).val() == 'enable') {
            $(".activated-option").removeClass('hidden');
        } else {
            $(".activated-option").addClass('hidden');
        }
    });

    $("#network_form").submit(function (event) {

        event.preventDefault();

        // Hide error message
        var wireless = $("#wireless"),
            network = $("#network"),
            frequency = $("#frequency"),
            password = $("#password"),
            password_confirmation = $('#password_confirmation');


        // Beautify DOM
        hideMessage();
        displayMessage('info', $.i18n('wireless.saving'), $.i18n('wireless.working-saving'));
        disableButtons('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        $.ajax({
            type: "POST",
            url: "/wireless",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val(),
                wireless: wireless.val(),
                network: network.val(),
                frequency: frequency.val(),
                password: password.val(),
                password_confirmation: password_confirmation.val()
            },
            async: true,
            cache: false,
            timeout: 120000,
            success: function () {
                displayMessage('success', $.i18n('devices.success'), $.i18n('wireless.changes-saved'));
                enableButtons();
            },
            error: function (xhr, textStatus, errorThrown) {
                enableButtons();
            }
        });

    });
});