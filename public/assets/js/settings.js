$(function () {

    $("#admin").submit(function (event) {

        event.preventDefault();

        // Beautify DOM
        hideMessage();
        displayMessage('info', $.i18n('settings.updating'), $.i18n('settings.updating-text'));
        disableButtons('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        $.ajax({
            type: "PUT",
            url: "/settings/admin",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val(),
                name: $('input[name=name]').val(),
                email: $('input[name=email]').val(),
                language: $('select[name=language]').val(),
                password: $('input[name=password]').val(),
                password_confirmation: $('input[name=password_confirmation]').val()
            },
            async: true,
            cache: false,
            timeout: 120000,
            success: function () {
                displayMessage('success', $.i18n('devices.success'), $.i18n('settings.admin-successful'));
                enableButtons();
            },
            error: function (xhr, textStatus, errorThrown) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                enableButtons();

            }
        });

    });

    $("#ovpn").submit(function (event) {

        event.preventDefault();

        // Beautify DOM
        hideMessage();
        disableButtons('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));
        displayMessage('info', $.i18n('settings.verifying'), $.i18n('settings.verifying-text'));

        $.ajax({
            type: "PUT",
            url: "/settings/ovpn",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val(),
                username: $('input[name=ovpn_name]').val(),
                password: $('input[name=ovpn_password]').val()
            },
            async: true,
            cache: false,
            timeout: 120000,
            success: function () {
                displayMessage('success', $.i18n('devices.success'), $.i18n('settings.ovpn-updated'));
                enableButtons();
            },
            error: function (xhr, textStatus, errorThrown) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                enableButtons();
            }
        });

    });

    $("#form-protocol").submit(function (event) {

        event.preventDefault();

        // Beautify DOM
        hideMessage();
        displayMessage('info', $.i18n('settings.verifying'), $.i18n('settings.verifying-text'));
        disableButtons('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        $.ajax({
            type: "PUT",
            url: "/settings/protocol",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val(),
                protocol: $('#protocol option:selected').val()
            },
            async: true,
            cache: false,
            timeout: 120000,
            success: function () {
                displayMessage('success', $.i18n('devices.success'), $.i18n('settings.protocol-updated'));
                enableButtons();
            },
            error: function (xhr, textStatus, errorThrown) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                enableButtons();
            }
        });

    });
});