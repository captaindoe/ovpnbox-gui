$(function () {

    /**
     * Starta om
     */
    $("#reboot").submit(function (event) {
        event.preventDefault();

        // Beautify DOM
        displayMessage('info', $.i18n('system.rebooting'), $.i18n('system.reboot'));
        disableButtons();

        $.ajax({
            type: "POST",
            url: "/system/reboot",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val()
            },
            async: true,
            cache: false,
            timeout: 120000
        });
    });

    /**
     * Stäng av
     */
    $("#poweroff").submit(function (event) {
        event.preventDefault();

        // Beautify DOM
        displayMessage('info', $.i18n('system.poweroff'), $.i18n('system.poweroff-working'));
        disableButtons();

        $.ajax({
            type: "POST",
            url: "/system/poweroff",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val()
            },
            async: true,
            cache: false,
            timeout: 120000
        });
    });
});