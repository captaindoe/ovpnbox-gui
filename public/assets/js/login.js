$(function () {

    $("#username").focus();

    /**
     * Används när man ska ansluta. Antingen har personen valt 'Välj bäst server' eller så
     * har personen valt en specifik server.
     */
    $("#login").submit(function(event) {
        event.preventDefault();

        // Hide error message
        var error = username = $("#username"), password = $("#password"), remember = $("#remember"), rememberme = 'off';

        // Update button
        hideMessage();
        disableButtons('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        if(remember.prop('checked')) {
            rememberme = "on";
        }

        $.ajax({
            type: "POST",
            url:  "/login",
            headers: {
                "X-Requested-With": 'XMLHttpRequest'
            },
            data: {
                _token: $('input[name=_token]').val(),
                name: username.val(),
                password: password.val(),
                remember: rememberme
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                window.location = '/';
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                enableButtons();
            }
        });
    });

});
